package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.CreateLeadPage;
import com.autoBot.pages.HomePage;
import com.autoBot.pages.LoginPage;
import com.autoBot.pages.MyHomePage;
import com.autoBot.pages.MyLeadsPage;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations {
		// TODO Auto-generated method stub
		@BeforeTest
		public void setData() {
			testcaseName = "TC002_CreateLead";
			testcaseDec = "Create a Lead";
			author = "Prem";
			category = "smoke";
			excelFileName = "TC001";
		} 

		@Test(dataProvider="fetchData") 
		public void createLead(String uName, String pwd,String cName, String fName,String lName) {
			new LoginPage()
			.enterUserName(uName)
			.enterPassWord(pwd) 
			.clickLogin();			
			
			new HomePage()
			.clickCrmsfa();
			new MyHomePage()
			.clickLead();
			
			new MyLeadsPage()
			.clickCreateLead();
			
			new CreateLeadPage()
			.enterCompanyName(cName)
			.enterFirstName(fName)
			.enterLastName(lName);
			
			
	
			
		}
		
	}

