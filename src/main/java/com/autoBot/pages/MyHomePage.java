package com.autoBot.pages;

//import wrappers.Annotations;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {

	/*public  MyLeadsPage clickLead() {
		// TODO Auto-generated method stub
		driver.findElementByLinkText("Leads").click();
		return new MyLeadsPage();
	}*/
	
	public MyLeadsPage clickLead() {
		WebElement cllead = locateElement("Link", "Leads");
		click(cllead);
		return new MyLeadsPage();
	}

}
