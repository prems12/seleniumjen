package com.autoBot.pages;

import org.openqa.selenium.WebElement;

//import wrappers.Annotations;
import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {

	public CreateLeadPage enterCompanyName(String cName) {
		// TODO Auto-generated method stub
		//driver.findElementById("createLeadForm_companyName").sendKeys(cName);
		WebElement compname = locateElement("id", "createLeadForm_companyName");
		compname.sendKeys(cName);
		return this;
	}
	public CreateLeadPage enterFirstName(String fName) {
		//driver.findElementById("createLeadForm_firstName").sendKeys(fName);
		WebElement enterfn = locateElement("id","createLeadForm_firstName");
		enterfn.sendKeys(fName);
		return this;
		
	}
	
	public CreateLeadPage enterLastName(String lName) {
		//driver.findElementById("createLeadForm_lastName").sendKeys(lName);
		WebElement enterln = locateElement("id","createLeadForm_lastName");
		enterln.sendKeys(lName);
		return this;
	}
	
	public ViewLeadPage clickSubmit() {
		//driver.findElementByClassName("smallSubmit").click();
		WebElement clksubmit = locateElement("class","smallSubmit");
		click(clksubmit);
		return new ViewLeadPage();
	}

}
