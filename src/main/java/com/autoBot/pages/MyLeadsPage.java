package com.autoBot.pages;

import org.openqa.selenium.WebElement;

//import wrappers.Annotations;
import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations {

	/*public CreateLeadPage clickCreateLead() {
		// TODO Auto-generated method stub
		
		driver.findElementByLinkText("Create Lead").click();
		return new CreateLeadPage();
	}*/
	
	public CreateLeadPage clickCreateLead() {
		WebElement clickcrlead = locateElement("link", "Create Lead");
		click(clickcrlead);
		return new CreateLeadPage();
	}

}
